"use client"
// import * as jQuery from 'jquery';
// import $ from 'jquery';
// import 'jquery-ui-dist/jquery-ui'
import $ from 'jquery';




const Header = () => {
    return ( 
<>
  

  <div className="myheader">
    <div className="wrap2 navbar-custom navbar-fixed-top">
      <div className="container">
        <div className="row">
          <div className="col-md-1 logo_div wow fadeIn" style={{ marginTop: '15px' }}>
            <a href="index.html">
              <img src="images/mainlogo.png" className="img-responsive" alt=""  style={{ transform: 'scale(2.5)', transition: 'transform 0.10s ease' }} />
            </a>
             {/* <p style={{ marginLeft: '10px', fontSize: '14px', color: '#000000' }}>grow@medicuspractice.at</p> */}
          </div>
          <div className="col-md-6" >
            <div className="droopmenu-navbar">
              <div className="droopmenu-inner">
                <div className="droopmenu-header">
                  <a href="#" className="droopmenu-brand visible-sm visible-xs">
                    <img
                      src="images/logo.png"
                      className="img-responsive center-block"
                      alt=""
                    />
                  </a>
                  <a href="#" className="droopmenu-toggle" />
                </div>
                {/* droopmenu-header */}
                <div className="droopmenu-nav">
                  <ul className="droopmenu" style={{marginLeft:70}}>
                    <li>
                      {" "}
                      <a href="/">Our Services</a>
                     </li>
                    <li>
                      {" "}
                      <a href="/">Our Offer</a>{" "}
                    </li>
                    <li>
                      {" "}
                      <a href="/about">About Us</a>{" "}
                    </li>
                    {/* <li>
                      {" "}
                      <a href="#">FAQ</a>{" "}
                    </li> */}
                  </ul>
                </div>
                {/* droopmenu-nav */}
              </div>
              {/* droopmenu-inner */}
            </div>
            {/* droopmenu-navbar  */}
          </div>
          <div className="col-md-5">
            <ul className="credentials"style={{marginTop:18}}>
              <li>
                <a
                  href="login_register.html"
                  data-toggle="tooltip"
                  data-placement="bottom"
                  title="Sign In/Create Account"
                >
                  <i className="las la-user" />
                </a>
              </li>
              {/* <li
                className="bell_wrap2 hidden-xs"
                id="cd-cart-trigger"
                // href="#0"
              >
                <a href="cd-img-replace">
                  <i className="las la-shopping-bag" />
                </a>
                <div className="c_badge"> 5 </div>
              </li> */}
              <li className="dropdown">
                {" "}
                <a
                  href="#"
                  className="dropdown-toggle hidden-xs"
                  data-toggle="dropdown"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i className="las la-globe-americas" />{" "}
                  <span className="caret" />
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a href="#">English (UK)</a>
                  </li>
                  <li>
                    <a href="#">English (USA)</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="https://form.jotform.com/233101901894048" className="inquire" target='https://form.jotform.com/233101901894048'>
                  Inquire Now
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="clearfix" />
      </div>
    </div>
  </div>
  <div className="desk_footer">
    <ul>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-user" />{" "}
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-shopping-bag" />
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-globe-americas" />{" "}
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-envelope" />{" "}
          </div>
        </a>
      </li>
    </ul>
  </div>
  <div className="modal fade advert_modal" id="myModal_login" role="dialog">
    <div className="modal-dialog">
      {/* Modal content*/}
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal">
            ×
          </button>
        </div>
        <div className="modal-body">
          <div className="login_wrap login_box">
            <ul className="nav nav-pills nav-justified">
              <li className="active">
                <a data-toggle="pill" href="#customer">
                  Login
                </a>
              </li>
              <li>
                <a data-toggle="pill" href="#agency">
                  Register
                </a>
              </li>
            </ul>
            <div className="tab-content">
              <div id="customer" className="tab-pane fade in active">
                <form action="/action_page.php" className="form_div_outer2">
                  <div className="form-group">
                    <label htmlFor="email">Email address:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Phone:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="pwd">Password:</label>
                    <input
                      type="password"
                      className="form-control"
                      id="pwd"
                    />{" "}
                  </div>{" "}
                  <a
                    href="javaScript:void(0);"
                    className="forgot"
                    data-toggle="collapse"
                    data-target="#fgot2"
                  >
                    Forgot Password?
                  </a>
                  <div id="fgot2" className="collapse">
                    <h6>
                      We shall send a re-activation link to your email address
                    </h6>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter Email Address"
                      />{" "}
                    </div>
                    <button className="btn btn_new1">Submit</button>
                  </div>
                  <div className="checkbox">
                    <label>
                      <input type="checkbox" /> Stay Logged In
                    </label>
                  </div>
                  <div className="login_lower">
                    <button type="submit" className="btn btn_new1">
                      Submit
                    </button>
                  </div>
                </form>
                <div className="clearfix" />
                <div className="reg_link">
                  <p>
                    If you dont have any account then register here -{" "}
                    <a href="#agency" className="link-to-tab">
                      Register
                    </a>
                  </p>
                </div>
              </div>
              <div id="agency" className="tab-pane fade">
                <form action="/action_page.php" className="form_div_outer2">
                  <div className="form-group">
                    <label htmlFor="email">Name:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Email address:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Phone:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="phone"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="pwd">Password:</label>
                    <input
                      type="password"
                      className="form-control"
                      id="pwd"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="pwd">Confirm Password:</label>
                    <input
                      type="password"
                      className="form-control"
                      id="pwd"
                    />{" "}
                  </div>
                  <div className="login_lower">
                    <button type="submit" className="btn btn_new1">
                      Submit
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn_new1" data-dismiss="modal">
            Close
          </button>
        </div>
      </div>
    </div>
  </div>
  <div className="scroll-top-wrapper">
    {" "}
    <span className="scroll-top-inner">
      <img src="images/return2.png" className="img-responsive" />
    </span>{" "}  
  </div>

  <script src="https://code.jquery.com/jquery-3.2.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	{/* <script type="text/javascript" src="js/custom.js"></script> */}
	<script type="text/javascript" src="js/droopmenu.js"></script>
	{/* <script type="text/javascript">
		jQuery(function ($) {
			$('.droopmenu-navbar').droopmenu({
				dmArrow: true
				, dmOffCanvas: true
				, dmOffCanvasPos: 'dmoffleft'
				, dmArrowDirection: 'dmarrowup'
			});
		});
	</script> */}
	
	<script src="js/wow.js"></script>
	<script>
		new WOW().init();
	</script>
	<script src="js/owl.carousel.js"></script>
	{/* <script type="text/javascript">
		$(document).ready(function () {
			var owl = $("#owl-demo");
			owl.owlCarousel({
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 3], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				autoPlay: 5000
				, rewindNav: true
				, rewindSpeed: 0
				, dots: true
				, itemsMobile: [479, 1] // itemsMobile disabled - inherit from itemsTablet option
			});
			// Custom Navigation Events
			$(".next").click(function () {
				owl.trigger('owl.next');
			})
			$(".prev").click(function () {
				owl.trigger('owl.prev');
			})
			$(".play").click(function () {
				owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
			})
			$(".stop").click(function () {
				owl.trigger('owl.stop');
			})
		});
	</script> */}

	<script src="js/waypoints.min.js" type="text/javascript"></script>
	<script src="js/jquery.counterup.min.js" type="text/javascript"></script>
	{/* <script>
		jQuery(document).ready(function ($) {
			$('.number_box span').counterUp({
				delay: 10, // the delay time in ms
				time: 1000 // the speed time in ms
			});
		});
	</script> */}
	
	
	{/* <script>
		var a = 0;
		$(window).scroll(function () {
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this)
						, countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 2000
						, easing: 'swing'
						, step: function () {
							$this.text(Math.floor(this.countNum));
						}
						, complete: function () {
							$this.text(this.countNum);
							//alert('finished');
						}
					});
				});
				a = 1;
			}
		});
	</script> */}
	
	
	
	
	
	
	
	
	
	
	
	
	
	{/* <script type="text/javascript">
		$(document).ready(function () {
			$(".mCustomScrollbar").mCustomScrollbar({
				axis: "x"
			});
		});
	</script> */}
	<script src="js/jquery.easing.min.js"></script>
	
	
	
	
	
	
	
	{/* <script type="text/javascript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</script> */}

	
	
	
	
	
	
	
	
	
	
	
	
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	{/* <script type="text/javascript">
		$(function () {
			$("#datepicker").datepicker({
				dateFormat: "dd-mm-yy"
				, duration: "fast"
			});
		});
	</script> */}
	
</>

     );
     
}

 
export default Header;

