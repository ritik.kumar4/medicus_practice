"use client"
import { BASE_ASSET_URL } from "@/app/utils";
import Script from "next/script";

const Ourservices = (props: { ourServicesdata: any }) => {
	// console.log(props.ourServicesdata);

	if (props.ourServicesdata.length === 0) {
		// Render default service boxes
		return (
		  <>
			<div className="service_green">
			  <div className="container concept_wrap" style={{ padding: 50 }}>
				<div className="row">
				  <div className="col-md-12 concept_top" style={{ marginTop: -130 }}>
					<h3>Our Services</h3>
					<p>
					  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
					  minim veniam, quis nostrud exercitation ullamco laboris nisi ut
					  aliquip ex ea commodo.
					</p>
				  </div>
				  <div className="clearfix" />
	
				  {/* Render default service boxes */}
				  <div className="col-md-4">
					<div className="service_box" data-uniform="true2">
					  <div className="sb_padding">
						<img src="images/i2.png" alt="" style={{ width: 50, height: 60 }} />
						<h4>Service 2</h4>
						<p>
						  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
						  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
						  enim ad minim veniam, quis nostrud exercitation ullamco laboris
						  nisi ut aliquip exea.{" "}
						</p>{" "}
						<a href="#">Read More</a>{" "}
					  </div>
					</div>
				  </div>
				  {/* Add similar blocks for other default services */}
	
				</div>
				<div className="clearfix" />
			  </div>
			</div>
		  </>
		);
	  }
	
  
	return (
	  <>
		<div className="service_green">
		  <div className="container concept_wrap" style={{ padding: 50 }}>
			<div className="row">
			  <div className="col-md-12 concept_top" style={{ marginTop: -130 }}>
				<h3>Our Services</h3>
				<p>
				  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
				  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
				  minim veniam, quis nostrud exercitation ullamco laboris nisi ut
				  aliquip ex ea commodo.
				</p>
			  </div>
			  <div className="clearfix" />
  
			  {props.ourServicesdata.map((service: any, index: number) => (
				<div className="col-md-4" key={index}>
				  <div className="service_box" data-uniform="true2">
					<div className="sb_padding">
					  {" "}
					  <img
						src={`${BASE_ASSET_URL}/${service.icon.filename}`}
						alt=""
						className="img-responsive"
					  />
					  <h4>{service.title}</h4>
					  <p>{service.description}</p>{" "}
					  <a href="#">Read More</a>{" "}
					</div>
				  </div>
				</div>
			  ))}
			</div>
			<div className="clearfix" />
		  </div>
		</div>
  
		<Script src="https://code.jquery.com/jquery-3.2.0.min.js"></Script>
		<Script src="js/bootstrap.min.js"></Script>
		{/* Add your other script tags here */}
	  </>
	);
  };
  
  export default Ourservices;