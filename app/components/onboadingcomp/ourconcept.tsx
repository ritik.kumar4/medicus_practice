"use client"

import Script from "next/script";

const Ourconcept = () => {
    return ( 
        <>
        <div className="container concept_wrap">
  <div className="row">
    <div className="col-md-12 concept_top " style={{marginTop:-50}}>
      <h3>Our Concept</h3>
      <h4>Get started as easy as 1, 2, 3</h4>
      <p>
        Yorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis
        molestie, dictum est a, mattis tellus. Sed dignissim, metus nec
        fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus
        elit sed risus. Maecenas eget condimentum velit, sit amet feugiat
        lectus. Class aptent taciti sociosqu ad litora torquent per conubia
        nostra, per inceptos himenaeos. Praesent auctor purus luctus enim
        egestas, ac scelerisque ante pulvinar.{" "}
      </p>
    </div>
    <div className="clearfix" />
    <div className="col-md-4">
      <div className="concept_box">
        <div className="concept_top">
          {" "}
          <img
            src="images/cp1.jpg"
            className="img-responsive center-block"
            alt=""
          />{" "}
        </div>
        <div className="concept_text" data-uniform="true">
          <div className="concept_padding">
            <h5>Concept 1</h5>
            <p>
              Lorem ipsum dolor sit amet consecte tur adipiscing elit semper
              dalar consectur elementum tempus hac.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div className="col-md-4">
      <div className="concept_box">
        <div className="concept_top">
          {" "}
          <img
            src="images/cp2.jpg"
            className="img-responsive center-block"
            alt=""
          />{" "}
        </div>
        <div className="concept_text" data-uniform="true">
          <div className="concept_padding">
            <h5>Concept 2</h5>
            <p>
              Lorem ipsum dolor sit amet consecte tur adipiscing elit semper
              dalar consectur elementum tempus hac.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div className="col-md-4">
      <div className="concept_box">
        <div className="concept_top">
          {" "}
          <img
            src="images/cp3.jpg"
            className="img-responsive center-block"
            alt=""
          />{" "}
        </div>
        <div className="concept_text" data-uniform="true">
          <div className="concept_padding">
            <h5>Concept 3</h5>
            <p>
              Lorem ipsum dolor sit amet consecte tur adipiscing elit semper
              dalar consectur elementum tempus hac.{" "}
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="clearfix" />
</div>

<Script src="https://code.jquery.com/jquery-3.2.0.min.js"></Script>
	<Script src="js/bootstrap.min.js"></Script>
	{/* <Script type="text/javaScript" src="js/custom.js"></Script> */}
	<Script type="text/javaScript" src="js/droopmenu.js"></Script>
	{/* <Script type="text/javaScript">
		jQuery(function ($) {
			$('.droopmenu-navbar').droopmenu({
				dmArrow: true
				, dmOffCanvas: true
				, dmOffCanvasPos: 'dmoffleft'
				, dmArrowDirection: 'dmarrowup'
			});
		});
	</Script> */}
	
	<Script src="js/wow.js"></Script>
	<Script>
		new WOW().init();
	</Script>
	<Script src="js/owl.carousel.js"></Script>
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			var owl = $("#owl-demo");
			owl.owlCarousel({
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 3], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				autoPlay: 5000
				, rewindNav: true
				, rewindSpeed: 0
				, dots: true
				, itemsMobile: [479, 1] // itemsMobile disabled - inherit from itemsTablet option
			});
			// Custom Navigation Events
			$(".next").click(function () {
				owl.trigger('owl.next');
			})
			$(".prev").click(function () {
				owl.trigger('owl.prev');
			})
			$(".play").click(function () {
				owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
			})
			$(".stop").click(function () {
				owl.trigger('owl.stop');
			})
		});
	</Script> */}

	<Script src="js/waypoints.min.js" type="text/javaScript"></Script>
	<Script src="js/jquery.counterup.min.js" type="text/javaScript"></Script>
	{/* <Script>
		jQuery(document).ready(function ($) {
			$('.number_box span').counterUp({
				delay: 10, // the delay time in ms
				time: 1000 // the speed time in ms
			});
		});
	</Script> */}
	
	
	{/* <Script>
		var a = 0;
		$(window).scroll(function () {
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this)
						, countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 2000
						, easing: 'swing'
						, step: function () {
							$this.text(Math.floor(this.countNum));
						}
						, complete: function () {
							$this.text(this.countNum);
							//alert('finished');
						}
					});
				});
				a = 1;
			}
		});
	</Script>
	 */}
	
	
	
	
	
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			$(".mCustomScrollbar").mCustomScrollbar({
				axis: "x"
			});
		});
	</Script> */}
	<Script src="js/jquery.easing.min.js"></Script>
	
	
	
	
	
	
{/* 	
	<Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	
	
	
	
	
	
	
	
	
	
	<Script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></Script>
	{/* <Script type="text/javaScript">
		$(function () {
			$("#datepicker").datepicker({
				dateFormat: "dd-mm-yy"
				, duration: "fast"
			});
		});
	</Script> */}
        </>
     );
}
 
export default Ourconcept;