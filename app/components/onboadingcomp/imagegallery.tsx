import { BASE_ASSET_URL } from "@/app/utils";
import Script from "next/script";

const Imagegallery = (props: { galleryImageData: any }) => {
  const { galleryImageData } = props;

  // Check if data is present
  if (galleryImageData && galleryImageData.length > 0) {
    return (
      <>
        <div className="service_green">
          <div className="container concept_wrap" style={{ padding: 50 }}>
            <div className="row">
              <div className="col-md-9 col-sm-9 concept_top" style={{ marginTop: -130 }}>
                <h3>Image Gallery</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
                  minim
                </p>
              </div>
              <div className="col-md-3 col-sm-3">
                <div className="form-group">
                  <select className="form-control" id="sel1">
                    <option>Select Location</option>
                    <option>Wien</option>
                  </select>
                </div>
              </div>
              <div className="clearfix" />
              <div className="col-md-12 info_bottom2">
                <ul className="nav nav-pills popular_pills">
                  {/* Your nav pills code */}
                </ul>
                <div className="tab-content">
                  <div id="home" className="tab-pane fade in active">
                    <div className="parent">
                      {galleryImageData.map((item: any, index: number) => (
                        <div key={index} className={`div${index + 1}`}>
                          <a href={item.image.path}>
                            <img
                              src={`${BASE_ASSET_URL}/${item.image.filename}`}
                              className="img-responsive center-block"
                              alt=""
                            />
                          </a>
                        </div>
                      ))}
                    </div>
                  </div>
                  {/* Your other tab panes */}
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </>
    );
  } else {
    // Render default content when no data is present
    return (
      <>
        <div className="service_green">
          <div className="container concept_wrap">
            <div className="row">
              <div className="col-md-9 col-sm-9 concept_top">
                <h3>Image Gallery</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
                  minim
                </p>
              </div>
              <div className="col-md-3 col-sm-3">
                <div className="form-group">
                  <select className="form-control" id="sel1">
                    <option>Select Location</option>
                    <option>Sydney</option>
                    <option>Perth</option>
                    <option>Adelaide</option>
                  </select>
                </div>
              </div>
              <div className="clearfix" />
              <div className="col-md-12 info_bottom2">
                <ul className="nav nav-pills popular_pills">
                  {/* Your nav pills code */}
                </ul>
                <div className="tab-content">
                  <div id="home" className="tab-pane fade in active">
                    <div className="parent">
                      {/* Your default content */}
                    </div>
                  </div>
                  {/* Your other tab panes */}
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </>
    );
  }
};

export default Imagegallery;
