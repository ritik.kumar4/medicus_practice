"use client"

import Script from "next/script";

const Whatmakes = () => {
    return ( 
        <>
        <div className="container concept_wrap" style={{padding:50}}>
  <div className="row">
    <div className="col-md-12 concept_top">
      <h3>What makes us different</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo.
      </p>
    </div>
    <div className="clearfix" />
    <div className="col-md-12 check_offer">
      <a href="https://form.jotform.com/233101901894048" className="btn btn_new1" target="https://form.jotform.com/233101901894048">
      Inquire Now
      </a>
    </div>
    <div className="clearfix" />
    <div className="col-md-12 what_table">
      <div className="table-responsive">
        <table className="table">
          <thead>
            <tr>
              <th />
              <th>
                Solo practice <br />
                (Insurance)
              </th>
              <th>
                Solo practice <br />
                (Private)
              </th>
              <th>
                PVZ <br />
                (Insurance)
              </th>
              <th className="light_yellow_bg">Medicus Practice</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="self_text">Self - employed</td>
              <td>
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />{" "}
              </td>
              <td>
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
              <td />
              <td className="light_yellow_bg">
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
            </tr>
            <tr>
              <td className="self_text">
                Economic and professional independence
              </td>
              <td> </td>
              <td>
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
              <td />
              <td className="light_yellow_bg">
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
            </tr>
            <tr>
              <td className="self_text">Sufficient time for patients</td>
              <td> </td>
              <td>
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
              <td />
              <td className="light_yellow_bg">
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
            </tr>
            <tr>
              <td className="self_text">Interdisciplinary Collaboration</td>
              <td> </td>
              <td />
              <td>
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
              <td className="light_yellow_bg">
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
            </tr>
            <tr>
              <td className="self_text">
                Reduced entrepreneurial risk &amp; cost reduction
              </td>
              <td> </td>
              <td />
              <td>
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
              <td className="light_yellow_bg">
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
            </tr>
            <tr>
              <td className="self_text">Relief from administrative tasks</td>
              <td> </td>
              <td />
              <td>
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
              <td className="light_yellow_bg">
                <img
                  src="images/tick.png"
                  className="img-responsive center-block"
                  alt=""
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div className="clearfix" />
</div>


<Script src="https://code.jquery.com/jquery-3.2.0.min.js"></Script>
	<Script src="js/bootstrap.min.js"></Script>
	{/* <Script type="text/javascript" src="js/custom.js"></Script> */}
	<Script type="text/javascript" src="js/droopmenu.js"></Script>
	{/* <Script type="text/javascript">
		jQuery(function ($) {
			$('.droopmenu-navbar').droopmenu({
				dmArrow: true
				, dmOffCanvas: true
				, dmOffCanvasPos: 'dmoffleft'
				, dmArrowDirection: 'dmarrowup'
			});
		});
	</Script> */}
	
	<Script src="js/wow.js"></Script>
	<Script>
		new WOW().init();
	</Script>
	<Script src="js/owl.carousel.js"></Script>
	{/* <Script type="text/javascript">
		$(document).ready(function () {
			var owl = $("#owl-demo");
			owl.owlCarousel({
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 3], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				autoPlay: 5000
				, rewindNav: true
				, rewindSpeed: 0
				, dots: true
				, itemsMobile: [479, 1] // itemsMobile disabled - inherit from itemsTablet option
			});
			// Custom Navigation Events
			$(".next").click(function () {
				owl.trigger('owl.next');
			})
			$(".prev").click(function () {
				owl.trigger('owl.prev');
			})
			$(".play").click(function () {
				owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
			})
			$(".stop").click(function () {
				owl.trigger('owl.stop');
			})
		});
	</Script> */}

	<Script src="js/waypoints.min.js" type="text/javascript"></Script>
	<Script src="js/jquery.counterup.min.js" type="text/javascript"></Script>
	{/* <Script>
		jQuery(document).ready(function ($) {
			$('.number_box span').counterUp({
				delay: 10, // the delay time in ms
				time: 1000 // the speed time in ms
			});
		});
	</Script> */}
	
	
	{/* <Script>
		var a = 0;
		$(window).scroll(function () {
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this)
						, countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 2000
						, easing: 'swing'
						, step: function () {
							$this.text(Math.floor(this.countNum));
						}
						, complete: function () {
							$this.text(this.countNum);
							//alert('finished');
						}
					});
				});
				a = 1;
			}
		});
	</Script> */}
	
	
	
	
	
	
	
	
	
	
	
	
{/* 	
	<Script type="text/javascript">
		$(document).ready(function () {
			$(".mCustomScrollbar").mCustomScrollbar({
				axis: "x"
			});
		});
	</Script> */}
	<Script src="js/jquery.easing.min.js"></Script>
	
	
	
	
	
	
	
	{/* <Script type="text/javascript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	{/* <Script type="text/javascript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true2]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true2]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	
	
	
	
	
	
	
	<Script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></Script>
	{/* <Script type="text/javascript">
		$(function () {
			$("#datepicker").datepicker({
				dateFormat: "dd-mm-yy"
				, duration: "fast"
			});
		});
	</Script> */}
        </>
     );
}
 
export default Whatmakes;