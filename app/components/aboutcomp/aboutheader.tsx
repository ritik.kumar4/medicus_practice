"use client"

const Aboutheader = () => {
    return ( 
      <>
  <div className="myheader header2">
    <div className="wrap2 navbar-custom navbar-fixed-top">
      <div className="container abt_container_size">
        <div className="row">
          <div className="col-md-1 logo_div">
            <a href="index.html">
              <img src="images/logo.png" className="img-responsive" alt="" />
            </a>
          </div>
          <div className="col-md-5">
            <div className="droopmenu-navbar">
              <div className="droopmenu-inner">
                <div className="droopmenu-header">
                  <a href="#" className="droopmenu-brand visible-sm visible-xs">
                    <img
                      src="images/mainlogo.png"
                      className="img-responsive center-block"
                      alt=""
                    />
                  </a>
                  <a href="#" className="droopmenu-toggle" />
                </div>
                {/* droopmenu-header */}
                <div className="droopmenu-nav">
                  <ul className="droopmenu" style={{display:"flex",gap:20}}>
                    <li>
                      {" "}
                      <a href="#">
                        {" "}
                        Our Practices <i className="las la-search" />
                      </a>{" "}
                    </li>
                    <li>
                      {" "}
                      <a href="#">Specializations &amp; Courses</a>{" "}
                    </li>
                    <li>
                      {" "}
                      <a href="#">For Health Profession</a>{" "}
                    </li>
                  </ul>
                </div>
                {/* droopmenu-nav */}
              </div>
              {/* droopmenu-inner */}
            </div>
            {/* droopmenu-navbar  */}
          </div>
          <div className="col-md-6">
            <ul className="credentials" style={{padding:20}}>
              <li>
                <a
                  href="login_register.html"
                  data-toggle="tooltip"
                  data-placement="bottom"
                  title="Sign In/Create Account"
                >
                  <i className="las la-user" />
                </a>
              </li>
              {/* <li
                className="bell_wrap2 hidden-xs"
                id="cd-cart-trigger"
                // href="#0"
              >
                <a href="cd-img-replace">
                  <i className="las la-shopping-bag" />
                </a>
                <div className="c_badge"> 5 </div>
              </li> */}
              <li className="dropdown">
                {" "}
                <a
                  href="#"
                  className="dropdown-toggle hidden-xs"
                  data-toggle="dropdown"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i className="las la-globe-americas" />{" "}
                  <span className="caret" />
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a href="#">English (UK)</a>
                  </li>
                  <li>
                    <a href="#">English (USA)</a>
                  </li>
                </ul>
              </li>
              {/* <li>
                <i className="las la-phone" /> +43 66493102300
              </li> */}
              <li>

                <a href="#" className="inquire">
                  Book an Appointment
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="clearfix" />
      </div>
    </div>
  </div>
  <div className="desk_footer">
    <ul>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-user" />{" "}
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-shopping-bag" />
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-globe-americas" />{" "}
          </div>
        </a>
      </li>
      <li>
        <a href="#">
          <div className="icon_div">
            {" "}
            <i className="las la-envelope" />{" "}
          </div>
        </a>
      </li>
    </ul>
  </div>
  
</>
     );
}
 
export default Aboutheader;